<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableConfrontos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confrontos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chave_id')->unsigned();
            $table->integer('time1_id')->unsigned();
            $table->integer('time2_id')->unsigned();
            $table->integer('pontostime1')->nullable();
            $table->integer('pontostime2')->nullable();
            $table->integer('campeonato_id')->unsigned();
            $table->timestamps();

        });
        
        Schema::table('confrontos', function (Blueprint $table){

            $table->foreign('chave_id')->references('id')->on('chaves')->onDelete('cascade');

            $table->foreign('time1_id')->references('id')->on('times')->onDelete('cascade');

            $table->foreign('time2_id')->references('id')->on('times')->onDelete('cascade');

            $table->foreign('campeonato_id')->references('id')->on('campeonatos')->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confrontos');
    }
}
