<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Chave;


class InsertChavesConstantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //BLOCO DAS OITAVAS
        //
        $chave1=new Chave;
        $chave1->descricao='Oitavas_1';
        $chave1->save();

        $chave2=new Chave;
        $chave2->descricao='Oitavas_2';
        $chave2->save();

        $chave3=new Chave;
        $chave3->descricao='Oitavas_3';
        $chave3->save();

        $chave4=new Chave;
        $chave4->descricao='Oitavas_4';
        $chave4->save();

        $chave5=new Chave;
        $chave5->descricao='Oitavas_5';
        $chave5->save();

        $chave6=new Chave;
        $chave6->descricao='Oitavas_6';
        $chave6->save();

        $chave7=new Chave;
        $chave7->descricao='Oitavas_7';
        $chave7->save();

        $chave8=new Chave;
        $chave8->descricao='Oitavas_8';
        $chave8->save();
        //
        //FIM DO BLOCO DAS OITAVAS
        



        //BLOCO DAS QUARTAS
        //
        $chave9=new Chave;
        $chave9->descricao='Quartas_1';
        $chave9->save();

        $chave10=new Chave;
        $chave10->descricao='Quartas_2';
        $chave10->save();
        
        $chave11=new Chave;
        $chave11->descricao='Quartas_3';
        $chave11->save();

        $chave12=new Chave;
        $chave12->descricao='Quartas_4';
        $chave12->save();
        //
        //FIM DO BLOCO DAS QUARTAS




        //BLOCO DAS SEMIS
        //
        $chave13=new Chave;
        $chave13->descricao='Semi_1';
        $chave13->save();

        $chave14=new Chave;
        $chave14->descricao='Semi_2';
        $chave14->save();
        //
        //FIM DO BLOCO DAS QUARTAS
        



        //FINAL
        $chave15=new Chave;
        $chave15->descricao='Final';
        $chave15->save();
        //
        



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
