<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTimeCampeonato extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campeonato_time', function (Blueprint $table) {
            $table->integer('time_id')->unsigned();
            $table->integer('campeonato_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('campeonato_time', function (Blueprint $table){

          $table->foreign('time_id')->references('id')->on('times')->onDelete('cascade');

          $table->foreign('campeonato_id')->references('id')->on('campeonatos')->onDelete('cascade');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('campeonato_time');
   }
}
