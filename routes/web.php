<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('/formtime', 'TimeController@formtime', function(){
	return redirect('/formtimes');
});

Route::put('/inserttime', 'TimeController@inserttime', function(){
	return redirect('/formtimes');
});

Route::get('/deletetime/{id}', 'TimeController@deletetime');

Route::get('/campeonato/{id}', 'CampeonatoController@viewTree');

Route::any('/formcampeonato', 'CampeonatoController@formCampeonato', function(){
	return redirect('/formtimes');
});

Route::put('/insertcampeonato', 'CampeonatoController@insertCampeonato', function(){
	return redirect('/formcampeonato');
});

Route::get('getAllTimes', 'TimeController@getAllTimes');

Route::get('/deletecampeonato/{id}', 'CampeonatoController@deletecampeonato');

Route::post('/updateConfrontos', 'ConfrontoController@updateConfrontos');
