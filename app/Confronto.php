<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Confronto extends Model
{
    protected $table = 'confrontos';

    public function campeonato(){
    	return $this->belongsTo('App\Campeonato');
    }
    public function time1(){
    	return $this->belongsTo('App\Time', 'time1_id');
    }

    public function time2(){
    	return $this->belongsTo('App\Time', 'time2_id');
    }

    public function chave(){
    	return $this->belongsTo('App\Chave');
    }

}
