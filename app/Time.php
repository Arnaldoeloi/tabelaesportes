<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $table = 'times';

    public function campeonatos(){
        return $this->belongsToMany('App\Campeonato');
    }

    public function confrontos(){
    	return $this->belongsToMany('App\Confronto', 'time1_id');
    }
}
