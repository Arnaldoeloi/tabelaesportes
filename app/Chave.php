<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Chave extends Model
{
    protected $table = 'chaves';


    public function confrontos(){
    	return $this->hasMany('App\Confronto');
    }
}
