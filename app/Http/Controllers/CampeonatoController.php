<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Campeonato;
use Illuminate\Support\Facades\DB;
use App\Time;
use App\Chave;
use App\Confronto;


class CampeonatoController extends Controller
{
    public function viewTree($id){
        $campeonato = Campeonato::find($id);
        $chaves= Chave::all();
        return view('viewcampeonato', ['campeonato' => $campeonato, 'chaves' =>$chaves]);
    }

	public function formCampeonato(){
		$campeonatos = DB::table('campeonatos')->get();
        $times = DB::table('times')->get();
		return view('formCampeonato', ['campeonatos' => $campeonatos, 'times' => $times]);
	}

    public function insertCampeonato(Request $request){
     	$campeonato = new Campeonato;
     	$campeonato->campeonato=$request->campeonatonome;
        $campeonato->save();

        $confronto = new Confronto;
        $confronto->campeonato_id=$campeonato->id;
        

        $cont=1;
        $chaves=1;
        $timesArray= array();
        $timesArray=$request->times;

        shuffle($timesArray);
        foreach ($timesArray as $time) {
            $time = Time::where('time', $time)->get()->First();
            if($cont==1){
                $confronto->pontostime1=0;
                $confronto->time1()->associate($time);
                $cont=$cont+1;
            }else if($cont==2){
                $confronto->pontostime2=0;
                $confronto->time2()->associate($time);
                $cont=1;
                $confronto->chave_id=$chaves;
                $chaves=$chaves+1;
                $confronto->campeonato_id=$campeonato->id;
                $confronto->save();
                $confronto=new Confronto;
            }
            $campeonato->times()->attach($time->id);   
        }
        return 1;
    }
    

    public function deletecampeonato($id){
        $campeonato = Campeonato::find($id);
        $campeonato->delete();
        return redirect('/formcampeonato');
    }
}
