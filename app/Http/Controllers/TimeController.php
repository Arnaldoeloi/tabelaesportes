<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Time;
use Illuminate\Support\Facades\DB;

class TimeController extends Controller
{
    public function formtime(){
    	$times = Time::all();

    	return view('formtime',['times' => $times]);
    }
     public function inserttime(Request $request){
     	$time = new Time;
     	$time->time=$request->timenome;
        
        $time->save();
        //redirect('/formtime') ;
        return redirect('/formtime');
        //view('formtime');
    }

    public function deletetime($id){
        $time = Time::find($id);
        foreach($time->campeonatos as $campeonato){
            $campeonato->delete();
        }
        $time->delete();
        return redirect('/formtime');
    }

    public function getAllTimes()
    {
        // essa função sera chamada em uma rota para retornar todos os times

        // ela faz parte da integração do angularJS
        
        $times = DB::table('times')->get();
        return json_encode($times);
    }

}
