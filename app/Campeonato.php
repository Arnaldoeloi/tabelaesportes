<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Campeonato extends Model
{
    protected $table = 'campeonatos';
   
    //public $times = times();

   // public $teste = 1;

    public function times()
    {
        return $this->belongsToMany('App\Time');
    }
    public function confrontos(){
    	return $this->hasMany('App\Confronto');
    }
}
