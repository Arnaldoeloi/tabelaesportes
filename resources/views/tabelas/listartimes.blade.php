<table>
  <tr>
    <th>Nome</th>
    <th>Excluir</th>
    <th>Campeonatos</th>
  </tr>

@foreach ($times as $time)
    
	  <tr>
	    <td>{{ $time->time }}</td>
	    <td><a href='{{ url("deletetime/$time->id") }}'><i class="fa fa-trash"></i></a></td>
	    
	    <td>
	    @foreach ($time->campeonatos as $campeonato)
			 |
	    <a href="{{ url("campeonato/$campeonato->id") }}">
			{{	$campeonato->campeonato.' '  }}
	    </a>
			| 
		@endforeach
	    </td>
	  </tr>
@endforeach
</table>