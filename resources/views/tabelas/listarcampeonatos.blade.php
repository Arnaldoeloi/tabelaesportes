
<table>
  <tr>
    <th>Nome</th>
    <th>Excluir</th>
    <th>Visualizar</th>
  </tr>

@foreach ($campeonatos as $campeonato)
    
	  <tr>
	    <td>{{ $campeonato->campeonato }}</td>
	    <td><a href="/deletecampeonato/{{$campeonato->id}}"><i class="fa fa-trash"></a></td>
	    <td><a href="/campeonato/{{$campeonato->id}}"><i class="fa fa-eye"></a></td>
	  </tr>
@endforeach
</table>
