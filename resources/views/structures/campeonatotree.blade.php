<!-- saved from url=(0041)file:///C:/Users/Arnaldo/Desktop/bla.html -->
   
   <div class="mw-parser-output" ng-app="arvoreApp" ng-controller='treeCtrl'>
   <a class="btn btn-info" href="/">Tabela Esportiva</a>
   <h1 style="font-size:40px">Campeonato: {{ $campeonato->campeonato }}</h1>

   <span style="font-size:20px" ng-hide="!hasVencedor"><span>Vencedor: </span><% vencedor %></span>

      <table  border="0" cellpadding="0" cellspacing="0" style="font-size: 90%; margin:1em 2em 1em 1em;"  width="72%">
         <tbody >
            <tr>
               <td height="5"></td>
               <td align="center" colspan="2" style="border:1px solid #aaa;" bgcolor="#F2F2F2"><button id="validaOitavas" ng-click="validarOitavas()">Finalizar Oitavas de final</button></td>
               <td colspan="2"></td>
               <td align="center" colspan="2" style="border:1px solid #aaa;" bgcolor="#F2F2F2"><button id="validaQuartas" ng-click="validarQuartas()">Finalizar Quartas de final</button></td>
               <td colspan="2"></td>
               <td align="center" colspan="2" style="border:1px solid #aaa;" bgcolor="#F2F2F2"><button id="validaSemi" ng-click="validarSemi()">Finalizar Semifinais</button></td>
               <td colspan="2"></td>
               <td align="center" colspan="2" style="border:1px solid #aaa;" bgcolor="#F2F2F2"><button id="validaFinal" ng-click="validarFinal()">Finalizar Final</button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td width="150">&nbsp;</td>
               <td width="30">&nbsp;</td>
               <td width="15">&nbsp;</td>
               <td width="20">&nbsp;</td>
               <td width="150">&nbsp;</td>
               <td width="30">&nbsp;</td>
               <td width="15">&nbsp;</td>
               <td width="20">&nbsp;</td>
               <td width="150">&nbsp;</td>
               <td width="30">&nbsp;</td>
               <td width="15">&nbsp;</td>
               <td width="20">&nbsp;</td>
               <td width="150">&nbsp;</td>
               <td width="30">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
               <td rowspan="4" style="border-width:0 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td rowspan="7" style="border-width:0 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td colspan="2" rowspan="3"></td>
               <td rowspan="7" style="border-width:0 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td rowspan="13" style="border-width:0 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td colspan="2" rowspan="9"></td>
               <td rowspan="13" style="border-width:0 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td rowspan="25" style="border-width:0 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td colspan="2" rowspan="21"></td>
            </tr>
            <tr>
               <td height="5"></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9" ng-click='teste'><% oitavas_11 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9" ><button ng-click="somar_oitavas1(1)"><% oitavas_11_pontos %></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9" ng-model='oitava_12'><% oitavas_12 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitavas1(2)"><% oitavas_12_pontos %></button></td>
               <td rowspan="6" style="border-width:2px 3px 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%quartas_11%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_quartas1(1)"><%quartas_11_pontos%></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="12" style="border-width:2px 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%quartas_12%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_quartas1(2)"><%quartas_12_pontos%></button></td>
               <td rowspan="12" style="border-width:2px 3px 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9" ><% oitavas_21 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9" ><button ng-click="somar_oitava_2(1)"><% oitavas_21_pontos %></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="6"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_22 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_2(2)"><% oitavas_22_pontos %></button></td>
               <td rowspan="6" style="border-width:2px 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%semi_11%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_semi1(1)"><%semi_11_pontos%></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="24" style="border-width:2px 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%semi_12%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_semi1(2)"><%semi_12_pontos%></button></td>
               <td rowspan="24" style="border-width:2px 3px 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_31 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_3(1)"><% oitavas_31_pontos %></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
               <td colspan="2" rowspan="18"></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_32 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_3(2)"><% oitavas_32_pontos %></button></td>
               <td rowspan="6" style="border-width:2px 3px 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%quartas_21%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_quartas2(1)"><%quartas_21_pontos%></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="12" style="border-width:2px 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%quartas_22%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_quartas2(2)"><%quartas_22_pontos%></button></td>
               <td rowspan="12" style="border-width:2px 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_41 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_4(1)"><% oitavas_41_pontos %></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="6"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_42 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_4(2)"><% oitavas_42_pontos %></button></td>
               <td rowspan="6" style="border-width:2px 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%final1%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_final(1)"><%final1_pontos%></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="23" style="border-width:2px 0 0 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%final2%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_final(2)"><%final2_pontos%></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_51 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_5(1)"><% oitavas_51_pontos %></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
               <td colspan="2" rowspan="10"></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_52 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_5(2)"><% oitavas_52_pontos %></button></td>
               <td rowspan="6" style="border-width:2px 3px 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%quartas_31%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_quartas3(1)"><%quartas_31_pontos%></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="12" style="border-width:2px 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%quartas_32%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_quartas3(2)"><%quartas_32_pontos%></button></td>
               <td rowspan="12" style="border-width:2px 3px 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_61 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_6(1)"><% oitavas_61_pontos %></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="6"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_62 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_6(2)"><% oitavas_62_pontos %></button></td>
               <td rowspan="6" style="border-width:2px 0 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%semi_21%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_semi2(1)"><%semi_21_pontos%></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="11" style="border-width:2px 0 0 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%semi_22%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_semi2(2)"><%semi_22_pontos%></button></td>
               <td rowspan="11" style="border-width:2px 0 0 0; border-style: solid;border-color:black;">&nbsp;</td>
               
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_71 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_7(1)"><% oitavas_71_pontos %></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
               <td colspan="2" rowspan="9"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_72 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_7(2)"><% oitavas_72_pontos %></button></td>
               <td rowspan="6" style="border-width:2px 3px 1px 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%quartas_41%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_quartas4(1)"><%quartas_41_pontos%></button></td>
               
               
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="2">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="5" style="border-width:2px 0 0 0; border-style: solid;border-color:black;">&nbsp;</td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><%quartas_42%></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_quartas4(2)"><%quartas_42_pontos%></button></td>
               <td rowspan="5" style="border-width:2px 0 0 0; border-style: solid;border-color:black;">&nbsp;</td>
               
               
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_81 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_8(1)"><% oitavas_81_pontos %></button></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td colspan="2" rowspan="3"></td>
               <td colspan="2" rowspan="3"></td>
            </tr>
            <tr>
               <td height="5"></td>
               <td rowspan="2" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><% oitavas_82 %></td>
               <td rowspan="2" align="center" style="border:1px solid #aaa;" bgcolor="#F9F9F9"><button ng-click="somar_oitava_8(2)"><% oitavas_82_pontos %></button></td>
               <td rowspan="2" style="border-width:2px 0 0 0; border-style: solid;border-color:black;">&nbsp;</td>
            </tr>
            <tr>
               <td height="5"></td>
            </tr>
         </tbody>
      </table>
      <!-- 
         NewPP limit report
         Parsed by mw1247
         Cached time: 20171028050943
         Cache expiry: 1900800
         Dynamic content: false
         CPU time usage: 0.012 seconds
         Real time usage: 0.017 seconds
         Preprocessor visited node count: 86/1000000
         Preprocessor generated node count: 0/1500000
         Post‐expand include size: 0/2097152 bytes
         Template argument size: 0/2097152 bytes
         Highest expansion depth: 2/40
         Expensive parser function count: 0/500
         -->
      <!--
         Transclusion expansion time report (%,ms,calls,template)
         100.00%    0.000      1 -total
         -->
   </div>
   <!-- Saved in parser cache with key ptwiki:pcache:idhash:868643-0!canonical and timestamp 20171028050943 and revision id 42368309
      -->
   
</div>
</body>
<script type="text/javascript">
/*
   BEM-VINDO AO SCRIPT DA NATUREZA




   AQUI SEUS SONHOS E DA MARINA SILVA SE TORNAM REALIDADE
   EU VI GNOMOS


 */


var app = angular.module('arvoreApp', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});


//CONTROLLER DA ARVORE   
app.controller("treeCtrl", function($scope, $http) {

   $scope.vencedor="";
   $scope.segundo="";








    $scope.oitavas_11="{{$campeonato->confrontos->where('chave_id', "1")->last()->time1->time}}";
    $scope.oitavas_11_pontos={{$campeonato->confrontos->where('chave_id', "1")->last()->pontostime1}};
    $scope.oitavas_12="{{$campeonato->confrontos->where('chave_id', "1")->last()->time2->time}}";
    $scope.oitavas_12_pontos={{$campeonato->confrontos->where('chave_id', "1")->last()->pontostime2}};

    $scope.oitavas_21="{{$campeonato->confrontos->where('chave_id', "2")->last()->time1->time}}";
    $scope.oitavas_21_pontos={{$campeonato->confrontos->where('chave_id', "2")->last()->pontostime1}};
    $scope.oitavas_22="{{$campeonato->confrontos->where('chave_id', "2")->last()->time2->time}}";
    $scope.oitavas_22_pontos={{$campeonato->confrontos->where('chave_id', "2")->last()->pontostime2}};

    $scope.oitavas_31="{{$campeonato->confrontos->where('chave_id', "3")->last()->time1->time}}";
    $scope.oitavas_31_pontos={{$campeonato->confrontos->where('chave_id', "3")->last()->pontostime1}};
    $scope.oitavas_32="{{$campeonato->confrontos->where('chave_id', "3")->last()->time2->time}}";
    $scope.oitavas_32_pontos={{$campeonato->confrontos->where('chave_id', "3")->last()->pontostime2}};

    $scope.oitavas_41="{{$campeonato->confrontos->where('chave_id', "4")->last()->time1->time}}";
    $scope.oitavas_41_pontos={{$campeonato->confrontos->where('chave_id', "4")->last()->pontostime1}};
    $scope.oitavas_42="{{$campeonato->confrontos->where('chave_id', "4")->last()->time2->time}}";
    $scope.oitavas_42_pontos={{$campeonato->confrontos->where('chave_id', "4")->last()->pontostime2}};

    $scope.oitavas_51="{{$campeonato->confrontos->where('chave_id', "5")->last()->time1->time}}";
    $scope.oitavas_51_pontos={{$campeonato->confrontos->where('chave_id', "5")->last()->pontostime1}};
    $scope.oitavas_52="{{$campeonato->confrontos->where('chave_id', "5")->last()->time2->time}}";
    $scope.oitavas_52_pontos={{$campeonato->confrontos->where('chave_id', "5")->last()->pontostime2}};

    $scope.oitavas_61="{{$campeonato->confrontos->where('chave_id', "6")->last()->time1->time}}";
    $scope.oitavas_61_pontos={{$campeonato->confrontos->where('chave_id', "6")->last()->pontostime1}};
    $scope.oitavas_62="{{$campeonato->confrontos->where('chave_id', "6")->last()->time2->time}}";
    $scope.oitavas_62_pontos={{$campeonato->confrontos->where('chave_id', "6")->last()->pontostime2}};

    $scope.oitavas_71="{{$campeonato->confrontos->where('chave_id', "7")->last()->time1->time}}";
    $scope.oitavas_71_pontos={{$campeonato->confrontos->where('chave_id', "7")->last()->pontostime1}};
    $scope.oitavas_72="{{$campeonato->confrontos->where('chave_id', "7")->last()->time2->time}}";
    $scope.oitavas_72_pontos={{$campeonato->confrontos->where('chave_id', "7")->last()->pontostime2}};

    $scope.oitavas_81="{{$campeonato->confrontos->where('chave_id', "8")->last()->time1->time}}";
    $scope.oitavas_81_pontos={{$campeonato->confrontos->where('chave_id', "8")->last()->pontostime1}};
    $scope.oitavas_82="{{$campeonato->confrontos->where('chave_id', "8")->last()->time2->time}}";
    $scope.oitavas_82_pontos={{$campeonato->confrontos->where('chave_id', "8")->last()->pontostime2}};


    $scope.quartas_11="@if( count($campeonato->confrontos->where('chave_id', "9") ) > 0) {{$campeonato->confrontos->where('chave_id', "9")->last()->time1->time}} @else Time vencedor @endif";

    $scope.quartas_11_pontos=@if(count($campeonato->confrontos->where('chave_id', "9")) > 0) {{$campeonato->confrontos->where('chave_id', "9")->last()->pontostime1}} @else 0 @endif;

    $scope.quartas_12="@if(count($campeonato->confrontos->where('chave_id', "9")) > 0) {{$campeonato->confrontos->where('chave_id', "9")->last()->time2->time}} @else Time vencedor @endif";

    $scope.quartas_12_pontos=@if(count($campeonato->confrontos->where('chave_id', "9")) > 0) {{$campeonato->confrontos->where('chave_id', "9")->last()->pontostime2}} @else 0 @endif;

    $scope.quartas_21="@if(count($campeonato->confrontos->where('chave_id', "10")) > 0) {{$campeonato->confrontos->where('chave_id', "10")->last()->time1->time}} @else Time vencedor @endif";
    $scope.quartas_21_pontos=@if(count($campeonato->confrontos->where('chave_id', "10")) > 0) {{$campeonato->confrontos->where('chave_id', "10")->last()->pontostime1}} @else 0 @endif;
    $scope.quartas_22="@if(count($campeonato->confrontos->where('chave_id', "10")) > 0) {{$campeonato->confrontos->where('chave_id', "10")->last()->time2->time}} @else Time vencedor @endif";
    $scope.quartas_22_pontos=@if(count($campeonato->confrontos->where('chave_id', "10")) > 0) {{$campeonato->confrontos->where('chave_id', "10")->last()->pontostime2}} @else 0 @endif;

    $scope.quartas_31="@if(count($campeonato->confrontos->where('chave_id', "11")) > 0) {{$campeonato->confrontos->where('chave_id', "11")->last()->time1->time}} @else Time vencedor @endif";
    $scope.quartas_31_pontos=@if(count($campeonato->confrontos->where('chave_id', "11")) > 0) {{$campeonato->confrontos->where('chave_id', "11")->last()->pontostime1}} @else 0 @endif;
    $scope.quartas_32="@if(count($campeonato->confrontos->where('chave_id', "11")) > 0) {{$campeonato->confrontos->where('chave_id', "11")->last()->time2->time}} @else Time vencedor @endif";
    $scope.quartas_32_pontos=@if(count($campeonato->confrontos->where('chave_id', "11")) > 0) {{$campeonato->confrontos->where('chave_id', "11")->last()->pontostime2}} @else 0 @endif;

    $scope.quartas_41="@if(count($campeonato->confrontos->where('chave_id', "12")) > 0) {{$campeonato->confrontos->where('chave_id', "12")->last()->time1->time}} @else Time vencedor @endif";
    $scope.quartas_41_pontos=@if(count($campeonato->confrontos->where('chave_id', "12")) > 0) {{$campeonato->confrontos->where('chave_id', "12")->last()->pontostime1}} @else 0 @endif;
    $scope.quartas_42="@if(count($campeonato->confrontos->where('chave_id', "12")) > 0) {{$campeonato->confrontos->where('chave_id', "12")->last()->time2->time}} @else Time vencedor @endif";
    $scope.quartas_42_pontos=@if(count($campeonato->confrontos->where('chave_id', "12")) > 0) {{$campeonato->confrontos->where('chave_id', "12")->last()->pontostime2}} @else 0 @endif;

    $scope.semi_11="@if(count($campeonato->confrontos->where('chave_id', "13")) > 0) {{$campeonato->confrontos->where('chave_id', "13")->last()->time1->time}} @else Time vencedor @endif";
    $scope.semi_11_pontos=@if(count($campeonato->confrontos->where('chave_id', "13")) > 0) {{$campeonato->confrontos->where('chave_id', "13")->last()->pontostime1}} @else 0 @endif;
    $scope.semi_12="@if(count($campeonato->confrontos->where('chave_id', "13")) > 0) {{$campeonato->confrontos->where('chave_id', "13")->last()->time2->time}} @else Time vencedor @endif";
    $scope.semi_12_pontos=@if(count($campeonato->confrontos->where('chave_id', "13")) > 0) {{$campeonato->confrontos->where('chave_id', "13")->last()->pontostime2}} @else 0 @endif;

    $scope.semi_21="@if(count($campeonato->confrontos->where('chave_id', "14")) > 0) {{$campeonato->confrontos->where('chave_id', "14")->last()->time1->time}} @else Time vencedor @endif";
    $scope.semi_21_pontos=@if(count($campeonato->confrontos->where('chave_id', "14")) > 0) {{$campeonato->confrontos->where('chave_id', "14")->last()->pontostime1}} @else 0 @endif;
    $scope.semi_22="@if(count($campeonato->confrontos->where('chave_id', "14")) > 0) {{$campeonato->confrontos->where('chave_id', "14")->last()->time2->time}} @else Time vencedor @endif";
    $scope.semi_22_pontos=@if(count($campeonato->confrontos->where('chave_id', "14")) > 0) {{$campeonato->confrontos->where('chave_id', "14")->last()->pontostime2}} @else 0 @endif;

    $scope.final1="@if(count($campeonato->confrontos->where('chave_id', "15")) > 0) {{$campeonato->confrontos->where('chave_id', "15")->last()->time1->time}} @else Time vencedor @endif";
    $scope.final1_pontos=@if(count($campeonato->confrontos->where('chave_id', "15")) > 0) {{$campeonato->confrontos->where('chave_id', "15")->last()->pontostime1}} @else 0 @endif;
    $scope.final2="@if(count($campeonato->confrontos->where('chave_id', "15")) > 0) {{$campeonato->confrontos->where('chave_id', "15")->last()->time2->time}} @else Time vencedor @endif";
    $scope.final2_pontos=@if(count($campeonato->confrontos->where('chave_id', "15")) > 0) {{$campeonato->confrontos->where('chave_id', "15")->last()->pontostime2}} @else 0 @endif;

    $scope.max=function(x,y){
      if(x>y){
         return x;
      }else{
         return y;
      }
    };



    

   
   $scope.confrontos = [
   @foreach ($campeonato->confrontos as $confronto)
            {
            'id':{{$confronto->id}},
            'chave_id':{{$confronto->chave_id}},
            'time1_id': {{$confronto->time1_id}},
            'time2_id': {{$confronto->time2_id}},
            'pontostime1':{{$confronto->pontostime1}},
            'pontostime2':{{$confronto->pontostime2}},
            'campeonato_id':{{$confronto->campeonato_id}}
            },
   @endforeach
   ];

    //funções para somar
    
     if($scope.confrontos.length==12){
         document.getElementById('validaOitavas').disabled=true;
     }else if($scope.confrontos.length==14){
         document.getElementById('validaOitavas').disabled=true;
         document.getElementById('validaQuartas').disabled=true;
     }else if($scope.confrontos.length==15){
         document.getElementById('validaOitavas').disabled=true;
         document.getElementById('validaQuartas').disabled=true;
         document.getElementById('validaSemi').disabled=true;
     }








     if($scope.final1_pontos>0 || $scope.final2_pontos>0 ){
       if($scope.max($scope.final1_pontos, $scope.final2_pontos)==$scope.final1_pontos){
          $scope.vencedor=$scope.final1;
          $scope.segundo=$scope.final2;
          $scope.hasVencedor=true;
       }else if($scope.final1_pontos == $scope.final2_pontos){
          $scope.vencedor='';
          $scope.segundo='';
          $scope.hasVencedor=false;
       }else{
          $scope.vencedor=$scope.final2;
          $scope.segundo=$scope.final1;
          $scope.hasVencedor=true;
       }
    }

    $scope.validarOitavas=function(){

         if($scope.max($scope.oitavas_11_pontos, $scope.oitavas_12_pontos)==$scope.oitavas_11_pontos){
            $scope.quartas_11=$scope.oitavas_11;

            $quarta1={
               'id' : -1,
               'chave_id':9,
               'time1_id' : $scope.confrontos[0].time1_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };

        
         }else{

            $scope.quartas_11=$scope.oitavas_12;
         
            $quarta1={
               'id' : -1,
               'chave_id':9,
               'time1_id' : $scope.confrontos[0].time2_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };

         }


         if($scope.max($scope.oitavas_21_pontos, $scope.oitavas_22_pontos)==$scope.oitavas_21_pontos){
         
            $scope.quartas_12=$scope.oitavas_21;


            $quarta1.time2_id = $scope.confrontos[1].time1_id;
            $scope.confrontos.push($quarta1);
         
         }else{
            $scope.quartas_12=$scope.oitavas_22;

            $quarta1.time2_id = $scope.confrontos[1].time2_id;
            $scope.confrontos.push($quarta1);
         }
         

         if($scope.max($scope.oitavas_31_pontos, $scope.oitavas_32_pontos)==$scope.oitavas_31_pontos){
            $scope.quartas_21=$scope.oitavas_31;

            $quarta2={
               'id' : -1,
               'chave_id':10,
               'time1_id' : $scope.confrontos[2].time1_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };

         }else{
            $scope.quartas_21=$scope.oitavas_32;

            $quarta2={
               'id' : -1,
               'chave_id':10,
               'time1_id' : $scope.confrontos[2].time2_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,

            };

         }



         if($scope.max($scope.oitavas_41_pontos, $scope.oitavas_42_pontos)==$scope.oitavas_41_pontos){
            $scope.quartas_22=$scope.oitavas_41;

            $quarta2.time2_id = $scope.confrontos[3].time1_id;
            $scope.confrontos.push($quarta2);

         }else{
            $scope.quartas_22=$scope.oitavas_42;

            $quarta2.time2_id = $scope.confrontos[3].time2_id;
            $scope.confrontos.push($quarta2);
         }


         if($scope.max($scope.oitavas_51_pontos, $scope.oitavas_52_pontos)==$scope.oitavas_51_pontos){
            $scope.quartas_31=$scope.oitavas_51;

            $quarta3={
               'id' : -1,
               'chave_id':11,
               'time1_id' : $scope.confrontos[4].time1_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };

         }else{
            $scope.quartas_31=$scope.oitavas_52;

            $quarta3={
               'id' : -1,
               'chave_id':11,
               'time1_id' : $scope.confrontos[4].time2_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };

         }


         if($scope.max($scope.oitavas_61_pontos, $scope.oitavas_62_pontos)==$scope.oitavas_61_pontos){
            $scope.quartas_32=$scope.oitavas_61;

            $quarta3.time2_id = $scope.confrontos[5].time1_id;
            $scope.confrontos.push($quarta3);
         }else{
            $scope.quartas_32=$scope.oitavas_62;

            $quarta3.time2_id = $scope.confrontos[5].time2_id;
            $scope.confrontos.push($quarta3);

         }


         if($scope.max($scope.oitavas_71_pontos, $scope.oitavas_72_pontos)==$scope.oitavas_71_pontos){
            $scope.quartas_41=$scope.oitavas_71;

            $quarta4={
               'id' : -1,
               'chave_id':12,
               'time1_id' : $scope.confrontos[6].time1_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };


         }else{
            $scope.quartas_41=$scope.oitavas_72;

            $quarta4={
               'id' : -1,
               'chave_id':12,
               'time1_id' : $scope.confrontos[6].time2_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };

         }
         if($scope.max($scope.oitavas_81_pontos, $scope.oitavas_82_pontos)==$scope.oitavas_81_pontos){
            $scope.quartas_42=$scope.oitavas_81;

            $quarta4.time2_id = $scope.confrontos[7].time1_id;
            $scope.confrontos.push($quarta4);

         }else{
            $scope.quartas_42=$scope.oitavas_82;

            $quarta4.time2_id = $scope.confrontos[7].time2_id;
            $scope.confrontos.push($quarta4);
         }

         
         


         /*
         $scope.confrontos = {{ json_encode($campeonato->confrontos) }};
         console.log($scope.confrontos);
         
         $scope.confrontos[0].pontostime1 = $scope.oitavas_11_pontos;
         $scope.confrontos[0].pontostime2 = $scope.oitavas_12_pontos;

         $scope.confrontos[1].pontostime1 = $scope.oitavas_21_pontos;
         $scope.confrontos[1].pontostime2 = $scope.oitavas_22_pontos;
         */

         

          data = {
              'confrontos' : $scope.confrontos,
              '_token' : "{{ csrf_token() }}",
            '_method' : 'POST',
          };
            headers={
                'Content-Type': 'application/json'
            };
          angular.toJson(data);
          $http.post('/updateConfrontos', data)
              .then(function(data, status, headers, config)
              {
                  console.log(status + ' - ' + data.data);
                  location.reload(); 
              }, function errorCallback(response){
                  console.log(response.data);
          }); 
               
              
         


    };

    $scope.validarQuartas=function(){
         if($scope.max($scope.quartas_11_pontos, $scope.quartas_12_pontos)==$scope.quartas_11_pontos){
            $scope.semi_11=$scope.quartas_11;
            $semi1={
               'id' : -1,
               'chave_id':13,
               'time1_id' : $scope.confrontos[8].time1_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };
         }else{
            $scope.semi_11=$scope.quartas_12;
            $semi1={
               'id' : -1,
               'chave_id':13,
               'time1_id' : $scope.confrontos[8].time2_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };
         }


         if($scope.max($scope.quartas_21_pontos, $scope.quartas_22_pontos)==$scope.quartas_21_pontos){
            $scope.semi_12=$scope.quartas_21;
            
            $semi1.time2_id = $scope.confrontos[9].time1_id;
            $scope.confrontos.push($semi1);

         }else{

            $scope.semi_12=$scope.quartas_22;
            
            $semi1.time2_id = $scope.confrontos[9].time2_id;
            $scope.confrontos.push($semi1);

         }



         if($scope.max($scope.quartas_31_pontos, $scope.quartas_32_pontos)==$scope.quartas_31_pontos){
            $scope.semi_21=$scope.quartas_31;
            $semi2={
               'id' : -1,
               'chave_id':14,
               'time1_id' : $scope.confrontos[10].time1_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };
         }else{
            $scope.semi_21=$scope.quartas_32;
            
            $semi2={
               'id' : -1,
               'chave_id':14,
               'time1_id' : $scope.confrontos[10].time2_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };
         }


         if($scope.max($scope.quartas_41_pontos, $scope.quartas_42_pontos)==$scope.quartas_41_pontos){

            $scope.semi_22=$scope.quartas_41;

            $semi2.time2_id = $scope.confrontos[11].time1_id;
            $scope.confrontos.push($semi2);

         }else{

            $scope.semi_22=$scope.quartas_42;

            $semi2.time2_id = $scope.confrontos[11].time2_id;
            $scope.confrontos.push($semi2);

         }

         data = {
              'confrontos' : $scope.confrontos,
              '_token' : "{{ csrf_token() }}",
            '_method' : 'POST',
          };
            headers={
                'Content-Type': 'application/json'
            };
          angular.toJson(data);
          $http.post('/updateConfrontos', data)
              .then(function(data, status, headers, config)
              {
                  console.log(status + ' - ' + data.data);
                  location.reload(); 
              }, function errorCallback(response){
                  console.log(response.data);
          }); 


    };

    $scope.validarSemi=function(){
         if($scope.max($scope.semi_11_pontos, $scope.semi_12_pontos)==$scope.semi_11_pontos){
            $scope.final1=$scope.semi_11;

            $final1={
               'id' : -1,
               'chave_id':15,
               'time1_id' : $scope.confrontos[12].time1_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };

         }else{
            $scope.final1=$scope.semi_12;

            $final1={
               'id' : -1,
               'chave_id':15,
               'time1_id' : $scope.confrontos[12].time2_id,
               'time2_id' : 0,
               'pontostime1' : 0,
               'pontostime2' : 0,
               'campeonato_id' : $scope.confrontos[0].campeonato_id,
            };

         }


         if($scope.max($scope.semi_21_pontos, $scope.semi_22_pontos)==$scope.semi_21_pontos){
            $scope.final2=$scope.semi_21;

            $final1.time2_id = $scope.confrontos[13].time1_id;
            $scope.confrontos.push($final1);

         }else{
            $scope.final2=$scope.semi_22;

            $final1.time2_id = $scope.confrontos[13].time2_id;
            $scope.confrontos.push($final1);

         }

         data = {
              'confrontos' : $scope.confrontos,
              '_token' : "{{ csrf_token() }}",
            '_method' : 'POST',
          };
            headers={
                'Content-Type': 'application/json'
            };
          angular.toJson(data);
          $http.post('/updateConfrontos', data)
              .then(function(data, status, headers, config)
              {
                  console.log(status + ' - ' + data.data);
                  location.reload(); 
              }, function errorCallback(response){
                  console.log(response.data);
          }); 

    };

    $scope.validarFinal=function(){
      if($scope.max($scope.final1_pontos, $scope.final2_pontos)==$scope.final1_pontos){
            $scope.vencedor=$scope.final1;
            $scope.segundo=$scope.final2;
         }else{
            $scope.vencedor=$scope.final2;
            $scope.segundo=$scope.final1;
         }

         data = {
              'confrontos' : $scope.confrontos,
              '_token' : "{{ csrf_token() }}",
            '_method' : 'POST',
          };
            headers={
                'Content-Type': 'application/json'
            };
          angular.toJson(data);
          $http.post('/updateConfrontos', data)
              .then(function(data, status, headers, config)
              {
                  console.log(status + ' - ' + data.data);
                  location.reload(); 
              }, function errorCallback(response){
                  console.log(response.data);
          }); 
    };

    $scope.somar_final = function(x) {
       if(x == 1){

         $scope.final1_pontos = parseInt($scope.final1_pontos)+1;
         $scope.confrontos[14].pontostime1 =  $scope.final1_pontos;

       }else if (x == 2) {

         $scope.final2_pontos = parseInt($scope.final2_pontos)+1;
         $scope.confrontos[14].pontostime2 =  $scope.final2_pontos;

       }
    };

    $scope.somar_semi1 = function(x) {
       if(x == 1){

         $scope.semi_11_pontos = parseInt($scope.semi_11_pontos)+1;
         $scope.confrontos[12].pontostime1 =  $scope.semi_11_pontos;

       }else if (x == 2) {

         $scope.semi_12_pontos = parseInt($scope.semi_12_pontos)+1;
         $scope.confrontos[12].pontostime2 =  $scope.semi_12_pontos;

       }
    };

    $scope.somar_semi2 = function(x) {
       if(x == 1){

         $scope.semi_21_pontos = parseInt($scope.semi_21_pontos)+1;
         $scope.confrontos[13].pontostime1 =  $scope.semi_21_pontos;

       }else if (x == 2) {
         $scope.semi_22_pontos = parseInt($scope.semi_22_pontos)+1;
         $scope.confrontos[13].pontostime2 =  $scope.semi_22_pontos;
       }
    };




    $scope.somar_quartas1 = function(x) {
       if(x == 1){

         $scope.quartas_11_pontos = parseInt($scope.quartas_11_pontos)+1;
         $scope.confrontos[8].pontostime1 =  $scope.quartas_11_pontos;

       }else if (x == 2) {
         $scope.quartas_12_pontos = parseInt($scope.quartas_12_pontos)+1;
         $scope.confrontos[8].pontostime2 =  $scope.quartas_12_pontos;
       }
    };

    $scope.somar_quartas2 = function(x) {
       if(x == 1){

         $scope.quartas_21_pontos = parseInt($scope.quartas_21_pontos)+1;
         $scope.confrontos[9].pontostime1 =  $scope.quartas_21_pontos;

       }else if (x == 2) {
         $scope.quartas_22_pontos = parseInt($scope.quartas_22_pontos)+1;
         $scope.confrontos[9].pontostime2 =  $scope.quartas_22_pontos;
       }
    };

    $scope.somar_quartas3 = function(x) {
       if(x == 1){

         $scope.quartas_31_pontos = parseInt($scope.quartas_31_pontos)+1;
         $scope.confrontos[10].pontostime1 =  $scope.quartas_31_pontos;

       }else if (x == 2) {
         $scope.quartas_32_pontos = parseInt($scope.quartas_32_pontos)+1;
         $scope.confrontos[10].pontostime2 =  $scope.quartas_32_pontos;
       }
    };

    $scope.somar_quartas4 = function(x) {
       if(x == 1){

         $scope.quartas_41_pontos = parseInt($scope.quartas_41_pontos)+1;
         $scope.confrontos[11].pontostime1 =  $scope.quartas_41_pontos;

       }else if (x == 2) {
         $scope.quartas_42_pontos = parseInt($scope.quartas_42_pontos)+1;
         $scope.confrontos[11].pontostime2 =  $scope.quartas_42_pontos;
       }
    };








    $scope.somar_oitavas1 = function(x) {
       if(x == 1){

         $scope.oitavas_11_pontos = parseInt($scope.oitavas_11_pontos)+1;
         $scope.confrontos[0].pontostime1 = $scope.oitavas_11_pontos;
         

       }else if (x == 2) {
         $scope.oitavas_12_pontos = parseInt($scope.oitavas_12_pontos)+1;
         $scope.confrontos[0].pontostime2 = $scope.oitavas_12_pontos;
         
       }
    };

    $scope.somar_oitava_2 = function(x) {
       if(x == 1){

         $scope.oitavas_21_pontos = parseInt($scope.oitavas_21_pontos)+1;
         $scope.confrontos[1].pontostime1 = $scope.oitavas_21_pontos;

       }else if (x == 2) {
         $scope.oitavas_22_pontos = parseInt($scope.oitavas_22_pontos)+1;
         $scope.confrontos[1].pontostime2 = $scope.oitavas_22_pontos;
       }
    };

    $scope.somar_oitava_3 = function(x) {
       if(x == 1){

         $scope.oitavas_31_pontos = parseInt($scope.oitavas_31_pontos)+1;
         $scope.confrontos[2].pontostime1 = $scope.oitavas_31_pontos;

       }else if (x == 2) {
         $scope.oitavas_32_pontos = parseInt($scope.oitavas_32_pontos)+1;
         $scope.confrontos[2].pontostime2 = $scope.oitavas_32_pontos;
       }
    };

    $scope.somar_oitava_4 = function(x) {
       if(x == 1){

         $scope.oitavas_41_pontos = parseInt($scope.oitavas_41_pontos)+1;
         $scope.confrontos[3].pontostime1 = $scope.oitavas_41_pontos;

       }else if (x == 2) {
         $scope.oitavas_42_pontos = parseInt($scope.oitavas_42_pontos)+1;
         $scope.confrontos[3].pontostime2 = $scope.oitavas_42_pontos;
       }
    };

    $scope.somar_oitava_5 = function(x) {
       if(x == 1){

         $scope.oitavas_51_pontos = parseInt($scope.oitavas_51_pontos)+1;
         $scope.confrontos[4].pontostime1 = $scope.oitavas_51_pontos;

       }else if (x == 2) {
         $scope.oitavas_52_pontos = parseInt($scope.oitavas_52_pontos)+1;
         $scope.confrontos[4].pontostime2 = $scope.oitavas_52_pontos;
       }
    };

    $scope.somar_oitava_6 = function(x) {
       if(x == 1){

         $scope.oitavas_61_pontos = parseInt($scope.oitavas_61_pontos)+1;
         $scope.confrontos[6-1].pontostime1 = $scope.oitavas_61_pontos;

       }else if (x == 2) {
         $scope.oitavas_62_pontos = parseInt($scope.oitavas_62_pontos)+1;
         $scope.confrontos[6-1].pontostime2 = $scope.oitavas_62_pontos;
       }
    };

    $scope.somar_oitava_7 = function(x) {
       if(x == 1){

         $scope.oitavas_71_pontos = parseInt($scope.oitavas_71_pontos)+1;
         $scope.confrontos[7-1].pontostime1 = $scope.oitavas_71_pontos;

       }else if (x == 2) {
         $scope.oitavas_72_pontos = parseInt($scope.oitavas_72_pontos)+1;
         $scope.confrontos[7-1].pontostime2 = $scope.oitavas_72_pontos;
       }
    };

    $scope.somar_oitava_8 = function(x) {
       if(x == 1){

         $scope.oitavas_81_pontos = parseInt($scope.oitavas_81_pontos)+1;
         $scope.confrontos[8-1].pontostime1 = $scope.oitavas_81_pontos;

       }else if (x == 2) {
         $scope.oitavas_82_pontos = parseInt($scope.oitavas_82_pontos)+1;
         $scope.confrontos[8-1].pontostime2 = $scope.oitavas_82_pontos;
       }
    };


});

</script>



</html>