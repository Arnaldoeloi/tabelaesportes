<script type="text/javascript" src="{{ URL::asset('js/angular.min.js') }}">
</script>

<script type="text/javascript" src="{{ URL::asset('js/tabelaApp.js') }}">
</script>
</head>

<body ng-app="tabelaApp" ng-controller="myCtrl">
    
@include('header')
    <div class="flex-center">
        @include('tabelas/listarcampeonatos')

    </div>
    <div id="times" style="margin: auto; width: 25%; text-align: center;">
        <form method="POST">
            <h1><% time %></h1>
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" ng-model="token" name="_token" value="<?php echo csrf_token(); ?>">
            <label>Campeonato: </label><input type="text" name="campeonatonome" ng-model="campeonatonome">
            <button type="submit" class="btn btn-default" ng-click="insertData()"><i class="fa fa-plus"></i></button>
        </form>
        <div style="margin: auto; width: 50%;">
            @include('tabelas/dropmenutimes')
        </div>
        <div>
            <ol>
                <li ng-repeat="x in times">
                    <%x%>
                    <button ng-click="removeTime($index)"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>
                </li>
            </ol>
            <p><%errortext%></p>
        </div>
    </div>
</body>


</html>
