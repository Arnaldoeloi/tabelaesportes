@include('header')
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            html{
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            table{
                width: 80%;
                padding-bottom: 15px;
                margin-bottom: 15px;
                border-bottom: solid 1px black;
            }
            td{
                border: solid 1px black;
                text-align: center;
            }

            label{
                font-weight: 600;
            }

            a{
                color: black;
            }
            
        </style>
    </head>
    <body>
        
        <div class="flex-center position-ref">
            @include('tabelas/listartimes')
        </div>

        <div class="flex-center position-ref">
            <form action="/inserttime" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <label>Time: </label><input type="text" name="timenome">
                <button type="submit" class="btn btn-default"><i class="fa fa-plus"></i></button>
            </form>
        </div>
    </body>
</html>
