
var app = angular.module('tabelaApp', [], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    });

app.controller("myCtrl", function($scope, $http) {
    $scope.times = [];
    	$scope.insertData = function(){

    	    data = {
    	        'times' : $scope.times,
    	        '_token' : $scope.token,
    			'_method' : 'PUT',
    	        'campeonatonome' : $scope.campeonatonome
    	    };
            headers={
                'Content-Type': 'application/x-www-form-urlencoded'
            };
    	    angular.toJson(data);
            if($scope.times.length==16){

                $http.post('/insertcampeonato', data)
                    .then(function(data, status, headers, config)
                    {
                        console.log(status + ' - ' + data);
                        location.reload(); 
                    }, function errorCallback(response){
                        console.log(response.data);
                }); 
                }else if($scope.campeonatonome.length==0){
                    $scope.errortext = "O campeonato precisa ter um nome.";
                }else if($scope.times.length<16){
                    $scope.errortext = "Você só pode cadastrar um campeonato com 16 times, porr@! NEM MAIS, NEM MENOS";
                }else{
                    $scope.errortext = "Houve um erro na criação do campeonato.";
                }
    	        
    	}
    	

        $scope.addTime = function () {
        	console.log("AddTime()");
            $scope.errortext = "";
            if (!$scope.time) {return;}
            if ($scope.times.indexOf($scope.time) == -1 && $scope.times.length<16) {
                $scope.times.push($scope.time);

                console.log("Push!");
            } else if($scope.times.length==16){
                $scope.errortext = "Você só pode cadastrar 16 times, animal!";
            }
            else {
                $scope.errortext = "Este time já está no campeonato!";
            }
        }
        $scope.removeTime = function (x) {
            $scope.errortext = "";
            $scope.times.splice(x, 1);
        } 
});


